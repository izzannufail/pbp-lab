## 1. Apakah perbedaan antara JSON dan XML?
JSON (JavaScript Object Notation) adalah format pertukaran data yang ringan dan tidak bergantung pada bahasa. JSON didasarkan pada JavaScript dan mudah dimengerti dan dibuat. XML (Extensible markup language) didesin untuk membawa data, bukan untuk menampilkan data. XML adalah bahasa markup yang mendefinisikan beberapa aturan untuk encoding dokumen dalam format yang dapat dibaca oleh manusia tetapi tetap bersifat machine-readable.

- JSON didasarkan atas bahasa JavaScript, sedangkan XML dari SGML (Standard Generalized Markup Language)
- Array bisa digunakan pada JSON, tetapi tidak bisa pada XML
- JSON tidak menggunakan end tag, sedangkan XML mewajibkan adanya start dan end tags
- JSON hanya support UTF-8 encoding, sedangkan XML support lebih banyak
- Comments bisa ditulis pada JSON, tetapi tidak bisa pada XML

## 2. Apakah perbedaan antara HTML dan XML?
HTML adalah bahasa yang biasa digunakan pada situs web untuk dilihat oleh siapapun yang mempunyai koneksi internet. Tags adalah kata-kata antara <> dan teks standar yang terpisah dari kode HTML. Ini ditampilkan di halaman web dalam bentuk gambar, tabel, diagram, dan lain-lain.

- HTML tidak case sensitive, XML case sensitive
- HTML Memiliki tags yang sudah terdefinisi, sedangkan tags pada XML lebih fleksibel dan bisa didefinisikan sesuai kebutuhan programer
- HTML digunakan untuk menampilkan data, sedangkan XML untuk transfer data
- Menutup tags dalam HTML tidak diwajibkan, tetapi pada XML, menutup tags adalah hal yang wajib untuk dilakukan
- HTML adalah sebuah markup language, sedangkan XML adalah sebuah markup language standar yang mendefinisikan markup language lainnya
