import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        fontFamily: 'Arial'
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  TextEditingController controllerTitle = TextEditingController();
  TextEditingController controllerSummary = TextEditingController();
  TextEditingController controllerBody = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: Container(
            margin: const EdgeInsets.fromLTRB(15, 10, 0, 10),
            child: const Image(image: AssetImage('assets/logo.png'))
            ),
          title: const Text('iCovid - News'),
          backgroundColor: Colors.blueAccent[100],
        ),
        body: ListView(
          children: [
            Container(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                children: [
                  const Text(
                    "Submit an Article",
                    style: TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.bold
                    ),
                  ),

                  const Padding(padding: EdgeInsets.only(top: 20.0)),

                  TextField(
                    controller: controllerTitle,
                    decoration: InputDecoration(
                      labelText: "Article Title",
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20.0)
                      )
                    ),
                  ),

                  const Padding(padding: EdgeInsets.only(top: 20.0)),

                  TextField(
                    controller: controllerSummary,
                    maxLines: 2,
                    decoration: InputDecoration(
                      labelText: "Summary",
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20.0)
                      )
                    ),
                  ),

                  const Padding(padding: EdgeInsets.only(top: 20.0)),

                  TextField(
                    controller: controllerBody,
                    maxLines: 15,
                    decoration: InputDecoration(
                      labelText: "Body",
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20.0)
                      )
                    ),
                  ),

                  const Padding(padding: EdgeInsets.only(top: 20.0)),

                  ElevatedButton(
                    child: const Text(
                      "Submit Article",
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold
                      ),
                    ),
                    onPressed: () => showDialog(
                      context: context,
                      builder: (BuildContext context) => AlertDialog(
                        title: const Text('Article Submited!'),
                        content: Column(
                          children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Text("Title          : ${controllerTitle.text}\nSummary : ${controllerSummary.text}\nBody    : ${controllerBody.text}")
                            )
                          ],
                        ),
                        actions: <Widget>[
                          TextButton(
                            onPressed: () => Navigator.pop(context, 'OK'),
                            child: const Text('OK'),
                          ),
                        ],
                      ),
                    )
                  )
                ],
              ),
            ),
          ],
        )
      );
  }
}
