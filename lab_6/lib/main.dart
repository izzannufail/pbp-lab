import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          leading: Container(
            margin: const EdgeInsets.fromLTRB(15, 10, 0, 10),
            child: const Image(image: AssetImage('assets/logo.png'))
            ),
          title: const Text('iCovid - News'),
          backgroundColor: Colors.blueAccent[100],
        ),
        body: Container(
          margin: const EdgeInsets.fromLTRB(30, 0, 30, 0),
          child: ListView(
            children: [
              const Padding(
                padding: EdgeInsets.fromLTRB(8, 16, 8, 8),
                child: Text(
                  'Latest Post',
                  style: TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.bold
                  )
                ),
              ),              
              buildCard(
                'Vaksin Baru datang',
                'Indonesia menerima satu juta dosis vaksin AZ dari Australia',
                'November 15',
                'https://img.jakpost.net/c/2020/07/30/2020_07_30_101326_1596094865._large.jpg'
              ),
              const Padding(padding: EdgeInsets.all(30.0)),
              const Padding(
                padding: EdgeInsets.all(8.0),
                child: Text(
                  'Previous Posts',
                  style: TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.bold
                  )
                ),
              ), 
              buildCard(
                'Maraknya Hoax Covid',
                'Dengan berjalannya waktu, makin banyak kasus hoax covid di Indonesia',
                'November 14',
                'https://awsimages.detik.net.id/community/media/visual/2020/06/02/1e3fbc4b-bac3-4870-898e-1a093c39dbaa_169.png?w=700&q=90'
              ),
              buildCard(
                'Vaksinasi Lansia Kurang', 
                'Pencapaian vaksinasi pada lansia masih belum mencapai target', 
                'November 13',
                'https://media.nature.com/lw1024/magazine-assets/d41586-020-02856-7/d41586-020-02856-7_18462852.jpg'
              ),
              buildCard(
                'Tanda-Tanda Corona', 
                'Berikut ini gejala-gejala seorang yang mengidap virus corona', 
                'November 12',
                'https://www.malaria.id/storage/images/Article-4b-1_1615365999.jpg'
              ),
              const Padding(
                padding: EdgeInsets.fromLTRB(16, 16, 16, 16),
                child: Text(
                  'No more posts to show',
                  style: TextStyle(
                    fontSize: 15,
                    color: Colors.grey
                  ),
                  textAlign: TextAlign.center,
                ),
              )
            ],
          ),
        ),
      ),
      theme: ThemeData(
        fontFamily: 'Arial'
      )
    );
  }

  Card buildCard(String title, String summary, String date, String imageURL) {
    return Card(
      elevation: 4.0,
      child: Column(
        children: [
          SizedBox(
            height: 200.0,
            child: Ink.image(
              image: NetworkImage(imageURL),
              fit: BoxFit.cover,
            ),
          ),
          ListTile(
            title: Text(
              title,
              style: const TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.bold
              ),
            ),
            subtitle: Text(date),
          ),
          Container(
              padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
              alignment: Alignment.centerLeft,
              child: Text(summary),
          ),
          ButtonBar(
            children: [
              TextButton(
                  child: const Text('Read more'),
                  onPressed: () {/* ... */},
                ),
            ],
          )
        ],
      ),
    );
  }

}